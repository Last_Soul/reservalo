/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Metodos;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import usuarios.Alojamiento;
import usuarios.CambiarFormato;
import usuarios.Genius;
import usuarios.Regular;
import usuarios.Reservas;
import static usuarios.Reservas.reservas;
import usuarios.Usuarios;


/**
 *
 * @author LastSoul
 */
public class Metodos {
    public static ArrayList<Usuarios> usuarios=  new ArrayList<Usuarios>();
    public static ArrayList<Alojamiento> establecimientos=  new ArrayList<Alojamiento>();
    public static ArrayList<String> atracciones=  new ArrayList<String>();
    static Scanner sc = new Scanner(System.in);
   
    

public static void presentar(String ciudad_reserva, int personas_reserva, Date date_inicio, Date date_fin,Usuarios usuarioR){
    ArrayList<Alojamiento> establecimientos_sin_iguales=  new ArrayList<Alojamiento>();
    for(Alojamiento a:establecimientos){
        establecimientos_sin_iguales.add(a);
    }
    
    if(establecimientos_sin_iguales == null || establecimientos_sin_iguales.size() == 0){
        System.out.println("En estos momentos no tenemos alojamientos libres en el rango de fecha ingresado.\nPruebe mas tarde.\nGracias");
        
    }
    if(establecimientos_sin_iguales != null || establecimientos_sin_iguales.size() != 0){
        ArrayList<String> nreservas = new ArrayList<>();
            int n= 1;
            for(Alojamiento b:establecimientos_sin_iguales){
                if(b.getCiudad().equals(ciudad_reserva)&& b.getCapacidadHabitacion()==(personas_reserva)){
                    System.out.println("╔                                                               ╗");
                    System.out.println("Opcion: "+n);
                    System.out.println("Nombre: "+b.getNombreEstablecimiento());
                    System.out.println("Ciudad, País " +b.getCiudad()+" ,"+b.getPais());
                    String [] Coordenadas = b.getCoordenadas().split(";");
                    String Latitud = Coordenadas[0];
                    String Longitud = Coordenadas [1];
                    System.out.println("Latitud: "+Latitud);
                    System.out.println("Longitud: "+Longitud);
                    System.out.println("Habitaciones: "+ b.getNumeroHabitaciones()+" "+b.getTipo()+"(Por: $"+b.getPrecio()+") Capacidad: "+b.getCapacidadHabitacion());
                    System.out.println("╚                                                              ╝");
                    n +=1;
                    String dereserva = String.valueOf(n)+"/"+b.getNombreEstablecimiento()+"/"+b.getTipo()+"/"+b.getCiudad()+"/"+b.getPais()+"/"+b.getEstrellas()+"/"+b.getNumeroHabitaciones()+"/"+b.getCoordenadas()+"/"+b.getPrecio()+"/"+b.getCapacidadHabitacion()+"/"+b.getCamas()+"/"+b.getServicios()+"/"+b.getRuc();
                    nreservas.add(dereserva);
                }
            }
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            String fecha_inicio = sdf.format(date_inicio);
            String fecha_fin = sdf.format(date_fin);
            System.out.println("Estas son las opciones encontradas con respecto a su busqueda.\nDesea ordenar sus busquedas en un orden? 'Y',caso contrario digite 'N': [Y/N]");
            String opcionR = sc.nextLine();
            ArrayList<String> reservasOrden = new ArrayList<>();
            if (opcionR.toUpperCase().equals("Y")){
                System.out.println("Desea ordenar las opciones por: \na.Precio\nb.Estrellas");
                String opcionS=sc.nextLine();
                if(opcionS.equals("a")){
                    reservasOrden.clear();
                    ArrayList<Float> preci = new ArrayList<>();
                    for(String s:nreservas){
                        String [] k = s.split("/");
                        preci.add(Float.parseFloat(k[8]));
                    }
                    preci.sort((o1, o2) -> o1.compareTo(o2));
                    n=1;
                    for(Float p:preci){
                        for(String i:nreservas){
                            String [] k = i.split("/");
                            if(k[8].equals(Float.toString(p))){
                                System.out.println("╔                                                               ╗");
                                System.out.println("Opcion: "+n);
                                System.out.println("Nombre: "+k[1]);
                                System.out.println("Ciudad, País " +k[3]+" ,"+k[4]);
                                String [] Coordenadas = k[7].split(";");
                                String Latitud = Coordenadas[0];
                                String Longitud = Coordenadas [1];
                                System.out.println("Latitud: "+Latitud);
                                System.out.println("Longitud: "+Longitud);
                                System.out.println("Habitaciones: "+ k[6]+" "+k[2]+"(Por: $"+k[8]+") Capacidad: "+k[9]);
                                System.out.println("╚                                                              ╝");
                                k[0]=String.valueOf(n);
                                String str =  String.join("/", k);
                                reservasOrden.add(str);
                                n +=1;
                            }
                            
                        }
                        System.out.println("Entonces digite el numero de las posibles reservas para realizar su reserva.");
                String opcionVal = sc.nextLine();
                CrearReserva( ciudad_reserva,  personas_reserva,  date_inicio,  date_fin, usuarioR, reservasOrden, opcionVal, fecha_inicio, fecha_fin);
                    }
                }
                if(opcionS.equals("b")){//ordenar por estrellas
                    reservasOrden.clear();
                    String dereserva ="";
                    n = 1;
                    for(String s:nreservas){
                        String [] k = s.split("/");
                        if(k[5].equals("5")){
                            System.out.println("╔                                                               ╗");
                            System.out.println("Opcion: "+n);
                            System.out.println("Nombre: "+k[1]);
                            System.out.println("Ciudad, País " +k[3]+" ,"+k[4]);
                            String [] Coordenadas = k[7].split(";");
                            String Latitud = Coordenadas[0];
                            String Longitud = Coordenadas [1];
                            System.out.println("Latitud: "+Latitud);
                            System.out.println("Longitud: "+Longitud);
                            System.out.println("Habitaciones: "+ k[6]+" "+k[2]+"(Por: $"+k[8]+") Capacidad: "+k[9]);
                            System.out.println("╚                                                              ╝");
                            k[0]=String.valueOf(n);
                            String str =  String.join("/", k);
                            reservasOrden.add(str);
                            n +=1;
                        }
                        if(k[5].equals("4")){
                            System.out.println("╔                                                               ╗");
                            System.out.println("Opcion: "+n);
                            System.out.println("Nombre: "+k[1]);
                            System.out.println("Ciudad, País " +k[3]+" ,"+k[4]);
                            String [] Coordenadas = k[7].split(";");
                            String Latitud = Coordenadas[0];
                            String Longitud = Coordenadas [1];
                            System.out.println("Latitud: "+Latitud);
                            System.out.println("Longitud: "+Longitud);
                            System.out.println("Habitaciones: "+ k[6]+" "+k[2]+"(Por: $"+k[8]+") Capacidad: "+k[9]);
                            System.out.println("╚                                                              ╝");
                            k[0]=String.valueOf(n);
                            String str =  String.join("/", k);
                            reservasOrden.add(str);
                            n +=1;
                        }
                        if(k[5].equals("3")){
                            System.out.println("╔                                                               ╗");
                            System.out.println("Opcion: "+n);
                            System.out.println("Nombre: "+k[1]);
                            System.out.println("Ciudad, País " +k[3]+" ,"+k[4]);
                            String [] Coordenadas = k[7].split(";");
                            String Latitud = Coordenadas[0];
                            String Longitud = Coordenadas [1];
                            System.out.println("Latitud: "+Latitud);
                            System.out.println("Longitud: "+Longitud);
                            System.out.println("Habitaciones: "+ k[6]+" "+k[2]+"(Por: $"+k[8]+") Capacidad: "+k[9]);
                            System.out.println("╚                                                              ╝");
                            k[0]=String.valueOf(n);
                            String str =  String.join("/", k);
                            reservasOrden.add(str);
                            n +=1;
                        }
                        if(k[5].equals("2")){
                            System.out.println("╔                                                               ╗");
                            System.out.println("Opcion: "+n);
                            System.out.println("Nombre: "+k[1]);
                            System.out.println("Ciudad, País " +k[3]+" ,"+k[4]);
                            String [] Coordenadas = k[7].split(";");
                            String Latitud = Coordenadas[0];
                            String Longitud = Coordenadas [1];
                            System.out.println("Latitud: "+Latitud);
                            System.out.println("Longitud: "+Longitud);
                            System.out.println("Habitaciones: "+ k[6]+" "+k[2]+"(Por: $"+k[8]+") Capacidad: "+k[9]);
                            System.out.println("╚                                                              ╝");
                            k[0]=String.valueOf(n);
                            String str =  String.join("/", k);
                            reservasOrden.add(str);
                            n +=1;
                        }
                        if(k[5].equals("1")){
                            System.out.println("╔                                                               ╗");
                            System.out.println("Opcion: "+n);
                            System.out.println("Nombre: "+k[1]);
                            System.out.println("Ciudad, País " +k[3]+" ,"+k[4]);
                            String [] Coordenadas = k[7].split(";");
                            String Latitud = Coordenadas[0];
                            String Longitud = Coordenadas [1];
                            System.out.println("Latitud: "+Latitud);
                            System.out.println("Longitud: "+Longitud);
                            System.out.println("Habitaciones: "+ k[6]+" "+k[2]+"(Por: $"+k[8]+") Capacidad: "+k[9]);
                            System.out.println("╚                                                              ╝");
                            k[0]=String.valueOf(n);
                            String str =  String.join("/", k);
                            reservasOrden.add(str);
                            n +=1;
                        }
                    }
                    System.out.println("Entonces digite el numero de las posibles reservas para realizar su reserva.");
                String opcionVal = sc.nextLine();
                CrearReserva( ciudad_reserva,  personas_reserva,  date_inicio,  date_fin, usuarioR, reservasOrden, opcionVal, fecha_inicio, fecha_fin);
                }
            }
            if(opcionR.toUpperCase().equals("N")){
                reservasOrden.clear();
                for(String y:nreservas){
                    reservasOrden.add(y);
                }
                System.out.println("Entonces digite el numero de las posibles reservas para realizar su reserva.");
                String opcionVal = sc.nextLine();
                CrearReserva( ciudad_reserva,  personas_reserva,  date_inicio,  date_fin, usuarioR, reservasOrden, opcionVal, fecha_inicio, fecha_fin);
            }
            
            
    }
    
}
static void CrearReserva(String ciudad_reserva, int personas_reserva, Date date_inicio, Date date_fin,Usuarios usuarioR,ArrayList<String> reservasOrden,String opcionR,String fecha_inicio,String fecha_fin){
for(String s:reservasOrden){
                String[] p = s.split("/") ;
                if(opcionR.equals(p[0])){
                    System.out.println("Esta segur@ que desea realizar su reserva en "+p[1]+" por un valor de: $ "+p[8]+"?[Y/N]");
                    String confirma = sc.nextLine();
                    if(confirma.toUpperCase().equals("Y")){
                        Float precio = Float.parseFloat(p[8]);
                        int dias=(int)((date_fin.getTime()-date_inicio.getTime())/86400000);
                        precio = precio*dias;
                        if(usuarioR instanceof Genius){
                            precio = precio - (float)(0.1*precio);
                            System.out.println("Como usted es un viajero Genius, recibe un descuento del 10%. \nNuevo precio a pagar: "+precio);
                            reservas.add(new Reservas(usuarioR.getNombre(),obtenerEmail(usuarioR.getNombre()),obtenerContrasena(usuarioR.getNombre()),Integer.parseInt(p[12]),p[1],p[2],p[3],p[4],Integer.parseInt(p[5]),Integer.parseInt(p[6]),p[7],precio,Integer.parseInt(p[9]),Integer.parseInt(p[10]),p[11],ciudad_reserva,personas_reserva,fecha_inicio,fecha_fin));
                            Reservas.anadirReservas(new Reservas(usuarioR.getNombre(),obtenerEmail(usuarioR.getNombre()),obtenerContrasena(usuarioR.getNombre()),Integer.parseInt(p[12]),p[1],p[2],p[3],p[4],Integer.parseInt(p[5]),Integer.parseInt(p[6]),p[7],precio,Integer.parseInt(p[9]),Integer.parseInt(p[10]),p[11],ciudad_reserva,personas_reserva,fecha_inicio,fecha_fin));
                    }
                            reservas.add(new Reservas(usuarioR.getNombre(),obtenerEmail(usuarioR.getNombre()),obtenerContrasena(usuarioR.getNombre()),Integer.parseInt(p[12]),p[1],p[2],p[3],p[4],Integer.parseInt(p[5]),Integer.parseInt(p[6]),p[7],Float.parseFloat(p[8]),Integer.parseInt(p[9]),Integer.parseInt(p[10]),p[11],ciudad_reserva,personas_reserva,fecha_inicio,fecha_fin));
                            Reservas.anadirReservas(new Reservas(usuarioR.getNombre(),obtenerEmail(usuarioR.getNombre()),obtenerContrasena(usuarioR.getNombre()),Integer.parseInt(p[12]),p[1],p[2],p[3],p[4],Integer.parseInt(p[5]),Integer.parseInt(p[6]),p[7],Float.parseFloat(p[8]),Integer.parseInt(p[9]),Integer.parseInt(p[10]),p[11],ciudad_reserva,personas_reserva,fecha_inicio,fecha_fin));
                    }
                    
                
                }
            }
}
    
    public static String obtenerEmail(String usuario){
        for(Usuarios u:usuarios){
            if(u.getNombre().equals(usuario)){
                return u.getEmail();
                }
        }
        return null;
    }
    
    public static String obtenerContrasena(String usuario){
        for(Usuarios u:usuarios){
            if(u.getNombre().equals(usuario)){
                return u.getContrasena();
                }
        }
        return null;
    }
    
    public static String comprobarContra(String Logincontra) {
        for(Usuarios d:usuarios){
            if (d.getContrasena().equals(Logincontra)){
                return null;}
    }
        return "a";
    }
    
    public static String validarContrasena(String contrasena) {
        char clave;
        byte  contNumero = 0, contLetra=0;
        for (byte i = 0; i < contrasena.length(); i++) {//se separan los caracteres y registra en contadores
            clave = contrasena.charAt(i);
            String caract = String.valueOf(clave);
            if (caract.matches("[a-z]")||(caract.matches("[A-Z]"))) {//se compara el caracteres con un rango de datos
                contLetra++;
            } else if (caract.matches("[0-9]")) {
                contNumero++;
                }
            else{
                System.out.println("La contrasena posee caracteres no validos.");
                return null;
                }
        }
        if(contrasena.length()>=6 && contNumero >=1){
            return "La contrasena es valida";}
        return null;
    }
    
    public static String validarEmail(String email) {//se hace uso del pattern y mather para validar el email a traves de un modelo ejemplo, es decir requiere un "@" en el email
        // Patrón para validar el email
        Pattern pattern = Pattern.compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
        Matcher mather = pattern.matcher(email);
 
        if (mather.find() == true) {
            return "El email ingresado es válido.";  
        } 
        return null;
    }
    
    public static Usuarios comprobarUser(String usuario) {
        for(Usuarios d:usuarios){
            if (d.getNombre().equals(usuario)){
                return d;}
    }
        return null;
        
    }
        
    public static String validarFecha(int dia,String Mes,String año){
        ArrayList<String> meses_30 = new ArrayList<String>();
        meses_30.add("04"); meses_30.add("06"); meses_30.add("09");meses_30.add("11");
        ArrayList<String> meses_31 = new ArrayList<String>();
        meses_31.add("01"); meses_31.add("03"); meses_31.add("05");meses_31.add("07");meses_31.add("08");meses_31.add("10");meses_31.add("12");
        for(String m:meses_30){
            if(Mes.equals(m)){
                if(dia<=30){
                    return null;
                }
            }
        }
        for(String m:meses_31){
            if(Mes.equals(m)){
                if(dia<=31){
                    return null;
                }
            }
        }
        if(Mes.equals("02")){
            if(dia<=28){
                return null;
    }}
        return "a";
}  
    
    
    public static Date ComprobarDate(String fecha,Date fecha2) {
        String[] parts = fecha.split("/");
        String dia_part = parts[0];
        int dia = Integer.parseInt(dia_part);
        String mes = parts[1]; 
        String año = parts[2];
        while(validarFecha(dia, mes,año) != null){//validar fecha entrada
                        System.out.println("Ingrese una fecha valida");
                        fecha = sc.nextLine();
                        parts = fecha.split("/");
                        dia_part = parts[0];
                        dia = Integer.parseInt(dia_part);
                        mes = parts[1]; 
                        año = parts[2];
                    }
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        
        try {
            Date date = formatter.parse(fecha);
            float n= fecha2.compareTo(date);
            if(n<0){//feha actual antes que fecha reserva
                return date;
            }
            
        } catch (ParseException e) {
        }
        return null;}
    public static void leerDatos() throws IOException{
        ArrayList<Regular> regular=  Regular.leerArchivo("Regular.txt");
        ArrayList<String> atraccion=new ArrayList<String>();
        ArrayList<String> simplemaps=new ArrayList<String>();
        atraccion=leerArchivoAtracciones("atracciones.txt");
        for(String s:atraccion){
            atracciones.add(s);
        }
        //simplemaps=CambiarFormato.crearArrays("simplemaps-worldcities-basic.txt");
        for(Regular r: regular){
            usuarios.add(r);
        }
        ArrayList<Genius> genius=  Genius.leerArchivo("Genius.txt");
        for(Genius g: genius){
            usuarios.add(g);
        }
        ArrayList<Alojamiento> alojamientor=  Alojamiento.leerArchivo("Alojamiento.txt");
        for(Alojamiento a: alojamientor){
            establecimientos.add(a);
        }
        ArrayList<Reservas> reservasr=  Reservas.leerArchivo("Reservas.txt");
        for(Reservas a: reservasr){
            reservas.add(a);
        }
    }
    public static ArrayList leerArchivoAtracciones(String archivo) {
		// crea el flujo para leer desde el archivo
		File file = new File(archivo);
		ArrayList atraccion= new ArrayList<>();	
		Scanner scanner;
		try {
			//se pasa el flujo al objeto scanner
			scanner = new Scanner(file);
			while (scanner.hasNextLine()) {
				// el objeto scanner lee linea a linea desde el archivo
				String linea = scanner.nextLine();
				Scanner delimitar = new Scanner(linea);				
				delimitar.useDelimiter("\\s*,\\s*");
                                String str = delimitar.next()+";"+delimitar.next()+";"+delimitar.next()+";"+delimitar.next()+";"+delimitar.next();
                                atraccion.add(str);
			}
			//se cierra el ojeto scanner
			scanner.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return atraccion;
	}

    public static boolean isdigit(String cadena) {
        boolean resultado;

        try {
            Integer.parseInt(cadena);
            resultado = true;
        } catch (NumberFormatException excepcion) {
            resultado = false;
        }

        return resultado;
    }
 
}
