/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package resérvalo;

import Metodos.Metodos;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;
import usuarios.Alojamiento;
import usuarios.Reservas;
import usuarios.Genius;
import usuarios.Regular;
import usuarios.Usuarios;
/**
 *
 * @author Bryan Sanchez
 */
public class Resérvalo { //main
    Scanner sc = new Scanner(System.in);
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {
        
        // TODO code application logic here
        Resérvalo main = new Resérvalo();
        main.menu();
        
        
    }
    public void menu() throws IOException{
        Metodos.leerDatos();
        String opcion="";
        while(!opcion.equals("5")){
            
              System.out.println("---------------------------------------------\nBienvenido al sistema de reservas");
              System.out.println("╔                Menu                   ╗");
              System.out.println("║ 1. Registro de Usuarios               ║");
              System.out.println("║ 2. Registrar establecimientos         ║");
              System.out.println("║ 3. Realizar una reserva               ║");
              System.out.println("║ 4. Verificar mis reservas             ║");  
              System.out.println("║ 5. Salir                              ║");
              System.out.println("╚                                       ╝");
              System.out.print("Ingrese opcion: ");      
              opcion = sc.nextLine();
            switch (opcion){
                case "1"://registrar usuarios
                    
                    System.out.println("Ingrese su usuario: ");
                    String usuario = sc.nextLine();
                    while(Metodos.comprobarUser(usuario) != null){
                        System.out.println("Ese usuario ya esta registrado.\nIngrese un nuevo usuario: ");
                        usuario = sc.nextLine();
                    }
                    System.out.println("Ingrese su correo: ");
                    String email = sc.nextLine();
                    while(Metodos.validarEmail(email)== null){//validar email por matcher y pattern
                        System.out.println("Ingrese un correo valido: ");
                        email = sc.nextLine();
                    }
                    System.out.println("Ingrese una contrasena: ");
                    String contrasena = sc.nextLine();
                    while(Metodos.validarContrasena(contrasena)== null){//validar contrasena
                        System.out.println("Ingrese una contrasena valida: ");
                        contrasena = sc.nextLine();
                    }
                    System.out.println("Desea ser usuario Genius?[Y/N]\n El usuario Genius posee un descuento de 10% en sus reservas, pero debe dejar reseñas de su estadis y registar su tarjeta de credito desde este momento: ");
                    String resGenius = sc.nextLine();
                    if(resGenius.toUpperCase().equals("Y")){
                        System.out.println("Ingrese su tarjeta de credito: ");
                        String numeroTarjeta= sc.nextLine();
                        Metodos.usuarios.add(new Genius(usuario,email,contrasena,numeroTarjeta));
                        Genius.anadirGenius(new Genius(usuario,email,contrasena,numeroTarjeta));
                        break;
                        }
                        Metodos.usuarios.add(new Regular(usuario,email,contrasena));
                        Regular.anadirRegular(new Regular(usuario,email,contrasena));
                    break;
                case "2"://registrar establecimientos
                    System.out.println("Necesitamos comprobar su identidad, porfavor digite su usuario: ");
                    
                    String comprobarUsuario = sc.nextLine();
                    while(Metodos.comprobarUser(comprobarUsuario) == null){
                        System.out.println("El usuario no existe.\nIngrese un nuevo usuario: ");
                        comprobarUsuario = sc.nextLine();
                    }
                    System.out.println("Para ser establecimiento debe ingresar ciertos datos: \nEscriba su ruc:");
                    String ruc = sc.nextLine();
                    while(Metodos.isdigit(ruc)!= true){
                            System.out.println("Ingrese un ruc correcto.");
                            ruc= sc.nextLine();
                        }
                    System.out.println("Ingrese el nombre del Establecimiento que desea registrar: ");
                    String nombreEstablecimiento = sc.nextLine();
                    System.out.println("Ingrese el tipo de Establecimiento que desea registrar\na. Hotel\nb.Hostel\nc.Apartamento\nd.Pensión: ");
                    String tipoE = sc.nextLine(); //tipo de establecimiento
                    String tipo_establecimiento = "";
                    if (tipoE.equals("a")){tipo_establecimiento = "Hotel"; }
                    if (tipoE.equals("b")){tipo_establecimiento = "Hostel"; }
                    if (tipoE.equals("c")){tipo_establecimiento = "Apartamento"; }
                    if (tipoE.equals("d")){tipo_establecimiento = "Pension"; }
                    System.out.println("Ingrese la ciudad a la que pertenece el "+tipo_establecimiento+": ");
                    String ciudad = sc.nextLine();
                    String coordenadas = null ;
                    System.out.println("Ingrese el pais al que pertenece el "+tipo_establecimiento+": ");
                    String pais = sc.nextLine();
                    for(String i : Metodos.atracciones){
                        String[] k = i.split(";");
                        if(k[0].toUpperCase().equals(ciudad.toUpperCase())&& k[1].toUpperCase().equals(pais.toUpperCase())){
                            coordenadas = k[3]+";"+k[4];
                        }
                    }
                    System.out.println("Ingrese el numero de estrellas que los clientes le han dado: ");
                    String estrellas = sc.nextLine();
                    while(Metodos.isdigit(estrellas)!= true){
                            System.out.println("Ingrese un numero de estrellas correcto.");
                            estrellas= sc.nextLine();
                        }
                    System.out.println("A continuacion le pediremos los detalles de las habitaciones: ");
                    String opcionHab = "y";
                    while (opcionHab.equals("y")){
                        System.out.println("Ingrese el numero de habitaciones que va a registrar: ");
                        String numeroHab = sc.nextLine();
                        while(Metodos.isdigit(numeroHab)!= true){
                            System.out.println("Ingrese un numero de habitaciones correcto.");
                            numeroHab= sc.nextLine();
                        }
                        System.out.println("Que tipo de habitaciones son?\na.Sencillas\nb.Matrimoniales\nc.Dobles\nd.Suites\ne.Familiares");
                        String tipoHab=sc.next();
                        if (tipoHab.equals("a")){tipo_establecimiento = "Sencillas"; }
                        if (tipoHab.equals("b")){tipo_establecimiento = "Matrimoniales"; }
                        if (tipoHab.equals("c")){tipo_establecimiento = "Dobles"; }
                        if (tipoHab.equals("d")){tipo_establecimiento = "Suites"; }
                        if(tipoHab.equals("e")){tipo_establecimiento = "Familiares";}
                        System.out.println("Detalle el precio de las habitaciones: ");
                        String precio = sc.nextLine();
                        while(Metodos.isdigit(precio)!= true){
                            System.out.println("Ingrese un precio correcto.");
                            precio= sc.nextLine();
                        }
                        System.out.println("Cuál es la capacidad maxima de personas en esta/s habitacion/es? ");
                        String capacidad = sc.nextLine();
                        while(Metodos.isdigit(capacidad)!= true){
                            System.out.println("Ingrese una capacidad correcta.");
                            capacidad= sc.nextLine();
                        }
                        System.out.println("Escriba el numero de camas: ");
                        String camas = sc.nextLine();
                        while(Metodos.isdigit(camas)!= true){
                            System.out.println("Ingrese un numero de camas correcto.");
                            camas= sc.nextLine();
                        }
                        System.out.println("Escriba los servicios que incluye la habitacion: ");
                        System.out.println("Los servicios pueden ser: \nWifi-Bañera-Agua caliente-Aire acondicionado\nCalefaccion-Baño privado-Despertador-Servicio a la habitacion-Desayuno-Lavanderia\nPiscina-Tv cable-Jacuzzi-Sauna-Spa\n Ingreselos separados por un (;)");
                        String servicios = sc.nextLine();
                        String Email= Metodos.obtenerEmail(comprobarUsuario) ;
                        String Contrasena = Metodos.obtenerContrasena(comprobarUsuario);
                        Metodos.establecimientos.add(new Alojamiento(comprobarUsuario,Email,Contrasena,Integer.parseInt(ruc),nombreEstablecimiento,tipo_establecimiento,ciudad,pais,Integer.parseInt(estrellas),Integer.parseInt(numeroHab),coordenadas,Float.parseFloat(precio),Integer.parseInt(capacidad),Integer.parseInt(camas),servicios));
                        Alojamiento.anadirAlojamiento(new Alojamiento(comprobarUsuario,Email,Contrasena,Integer.parseInt(ruc),nombreEstablecimiento,tipo_establecimiento,ciudad,pais,Integer.parseInt(estrellas),Integer.parseInt(numeroHab),coordenadas,Float.parseFloat(precio),Integer.parseInt(capacidad),Integer.parseInt(camas),servicios));
                        System.out.println("Desea continuar registrando habitaciones?[Y/N] ");
                        opcionHab = sc.nextLine();
                    }      
                    break;
                case "3": 
                    System.out.println("Bienvenido.\nAntes de realizar una reserva nos gustaria comprobar su identidad.\nPorfavor ingrese su cuenta:");
                    String LoginUser = sc.nextLine();
                    while(Metodos.comprobarUser(LoginUser) == null){
                        System.out.println("El usuario no existe.\nIngrese un nuevo usuario: ");
                        LoginUser = sc.nextLine();
                    }
                    Usuarios u = Metodos.comprobarUser(LoginUser);
                    System.out.println("Digite su contraseña: ");
                    String Logincontra = sc.nextLine();
                    while(Metodos.comprobarContra(Logincontra) != null){
                        System.out.println("La contrasena no es la correcta.\nDigitela denuevo: ");
                        Logincontra = sc.nextLine();
                    }
                    java.util.Date fechaActual = new Date();
                    System.out.println (fechaActual);
                    System.out.println("Ingrese la fecha desde la cual quiere empezar su reserva (Día/Mes/Año): ");
                    String fecha_entrada = sc.nextLine();//fecha entrada
                    Date date_entrada = Metodos.ComprobarDate(fecha_entrada,fechaActual);
                    while(Metodos.ComprobarDate(fecha_entrada, fechaActual)== null){
                          System.out.println("Fecha no valida, es menor que la actual.");
                          System.out.println("Ingrese una fecha de inicio valida: ");
                          fecha_entrada = sc.nextLine();
                          date_entrada = Metodos.ComprobarDate(fecha_entrada,fechaActual);
                    }  
                    System.out.println("Ingrese la fecha en la cual desea terminar su reserva (Día/Mes/Año): ");
                    String fecha_salida = sc.nextLine();//fecha salida
                    Date date_salida = Metodos.ComprobarDate(fecha_salida,date_entrada);
                    while(Metodos.ComprobarDate(fecha_salida, date_entrada)== null){//ya se valido la de entrada, asi que se la puede usar
                          System.out.println("Fecha no valida, es menor que la de entrada.");
                          System.out.println("Ingrese una fecha de salida valida: ");
                          fecha_salida = sc.nextLine();
                          date_salida = Metodos.ComprobarDate(fecha_salida,date_entrada);
                    }  
                    System.out.println("Ingrese la ciudad en la cual desea realizar su reserva: ");
                    String ciudad_reserva = sc.nextLine();
                    System.out.println("Ingrese el numero de personas para la reserva: ");
                    String personas_reserva = sc.nextLine();
                    while(Metodos.isdigit(personas_reserva)!= true){
                        System.out.println("Ingrese un numero de personas correcto.");
                        personas_reserva= sc.nextLine();
                    }
                    Metodos.presentar(ciudad_reserva,Integer.parseInt(personas_reserva),date_entrada,date_salida,u);
                    
                    //mostrar resultado de la busqueda
                    
                    //Calificacion.pedirCalif(date_salida);
                    //Calificacion.mostrarCalificacion(reseña, usuario, contraseña, promEstrellas);
                    
                    break;
                case "4":    
                    System.out.println("Bienvenido.\nAntes de revisar sus reservas nos gustaria comprobar su identidad.\nPorfavor ingrese su cuenta:");
                    String loginU = sc.nextLine();
                    while(Metodos.comprobarUser(loginU) == null){
                        System.out.println("El usuario no existe.\nIngrese un nuevo usuario: ");
                        loginU = sc.nextLine();
                    }
                    Usuarios s = Metodos.comprobarUser(loginU);
                    System.out.println("Digite su contraseña: ");
                    String loginC = sc.nextLine();
                    while(Metodos.comprobarContra(loginC) != null){
                        System.out.println("La contrasena no es la correcta.\nDigitela denuevo: ");
                        loginC = sc.nextLine();
                    }
                    java.util.Date Hoy = new Date();
                    System.out.println (Hoy);
                    ArrayList<Reservas> reservasOpcion=  new ArrayList<Reservas>();
                    for(Reservas r:Reservas.reservas){
                        if(r.getNombre().equals(loginU)){
                            reservasOpcion.add(r);
                        }
                    }
                    if(reservasOpcion == null || reservasOpcion.size() == 0){
                        System.out.println("Usted no posee reservas.");
                    break;}
                    else{
                        System.out.println("Usted posee un total de :"+reservasOpcion.size()+" reservas");
                        int n= 1;
                        ArrayList<String> preservas = new ArrayList<>();
                        for(Reservas b:reservasOpcion){
                            System.out.println("Reserva # "+n);
                            System.out.println("Nombre: "+b.getNombreEstablecimiento());
                            System.out.println("Ciudad, País " +b.getCiudad()+" ,"+b.getPais());
                            String [] Coordenadas = b.getCoordenadas().split(";");
                            String Latitud = Coordenadas[0];
                            String Longitud = Coordenadas [1];
                            System.out.println("Latitud: "+Latitud);
                            System.out.println("Longitud: "+Longitud);
                            System.out.println("Habitaciones: "+ b.getNumeroHabitaciones()+" "+b.getTipo()+"(Por: $"+b.getPrecio()+") Capacidad: "+b.getCapacidadHabitacion()); 
                            String dereserva = String.valueOf(n)+"/"+b.getNombreEstablecimiento()+"/"+b.getTipo()+"/"+b.getCiudad()+"/"+b.getPais()+"/"+b.getEstrellas()+"/"+b.getNumeroHabitaciones()+"/"+b.getCoordenadas()+"/"+b.getPrecio()+"/"+b.getCapacidadHabitacion()+"/"+b.getCamas()+"/"+b.getServicios()+"/"+b.getRuc();
                            preservas.add(dereserva);
                            if(Metodos.ComprobarDate(b.getDate_fin(), Hoy)!= null){
                                if(s instanceof Genius){
                                    System.out.println("Usted "+b.getNombre()+" es un usuario Genius por lo cual debe calificar las siguientes reservas: ");
                                    String[]servicios = b.getServicios().split(";");
                                    int x= 0;
                                    while(x<servicios.length){
                                        System.out.println("Califique el servicio de "+servicios[x]+"\nEscriba un numero entre 1 y 5 (5= la mas alta calificacion, 1 = la mas baja)");
                                        String servici = sc.nextLine();
                                        x = x +1;
                                    }
                                }
                                else{
                                System.out.println("Las siguientes reservas ya pasaron. Desea calificarlas? [Y/N]");
                                    String opcioncal = sc.nextLine();
                                    if (opcioncal.equals("Y")){
                                        System.out.println("Usted "+b.getNombre()+" es un usuario Genius por lo cual debe calificar las siguientes reservas: ");
                                        String[]servicios = b.getServicios().split(";");
                                        int x= 0;
                                        while(x<servicios.length){
                                            System.out.println("Califique el servicio de "+servicios[x]);
                                            String servici = sc.nextLine();
                                            x = x +1;
                                    }
                                    }
                                }
                            
                            }
                        }
                        System.out.println("Digite el numero de reserva que desea revisar: ");
                        String opcio = sc.nextLine();
                        for(String r:preservas){
                            String [] p = r.split("/");
                            if(opcio.equals(p[0])){
                                System.out.println("Nombre del alojamiento: "+p[1]);
                                System.out.println("Tipo: "+p[2]);
                                System.out.println("Se encuentra en: Ciudad/Pais"+p[3]+"/"+p[4]);
                                System.out.println("Posee: "+p[5]+" Estrellas");
                                System.out.println("Tiene "+p[6]+" habitaciones libres en estos momentos");
                                String [] presentarCoorde = p[7].split(";");
                                System.out.println("Se encuentra en: Latitud: "+presentarCoorde[0]+" ,Longitud: "+presentarCoorde[1]);
                                System.out.println("El precio es de: "+p[8]+ " por noche.");
                                String [] servi = p[9].split(";");
                                System.out.println("Posee los siguientes servicios: ");
                                for(String j:servi){
                                    System.out.println("- "+j);
                                }
                                //int estrellas,int numeroHabitaciones, String coordenadas, float precio, int capacidadHabitacion,int camas,String servicios
                                //Integer.parseInt(p[12]),p[2],p[3],p[4],Integer.parseInt(p[5]),Integer.parseInt(p[6]),p[7],precio,Integer.parseInt(p[9]),Integer.parseInt(p[10]),p[11],ciudad_reserva,personas_reserva,fecha_inicio,fecha_fin));
                            }
                        }
                        
                    
                    }
                    break;
                default:
                    System.out.println("Opcion No valida!!");
            }
        }
        sc.close();
    
    }

}