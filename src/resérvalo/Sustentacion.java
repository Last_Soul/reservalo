/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package resérvalo;

import static Metodos.Metodos.establecimientos;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;
import usuarios.Alojamiento;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

/**
 *
 * @author Estudiante
 */
public class Sustentacion {
    static Scanner sc = new Scanner(System.in);
    public static ArrayList<Double> orden=  new ArrayList<Double>();
    public static ArrayList<String> leercsv=  new ArrayList<String>();
    
    public static final String SEPARATOR=",";
    public static void main(String[] args) throws IOException{
        //lectura de csv
        BufferedReader br = null;
      
      try {
         
         br =new BufferedReader(new FileReader("stations.csv"));
         String line = br.readLine();
         while (null!=line) {
            String [] fields = line.split(SEPARATOR);
            String campos = fields.toString();
            leercsv.add(campos);
            System.out.println(Arrays.toString(fields));
            
            
            line = br.readLine();
         }
         
      } catch (Exception e) {
         
      } finally {
         if (null!=br) {
            br.close();
         }
         //fin lectura csv
        
        ArrayList<Alojamiento> alojamientor=  Alojamiento.leerArchivo("Alojamiento.txt");//leer alojamientos
        for(Alojamiento a: alojamientor){
            establecimientos.add(a);
        }
        for(Alojamiento b:establecimientos){
            System.out.println("Nombre: "+b.getNombreEstablecimiento());
            System.out.println("Ciudad, País " +b.getCiudad()+" ,"+b.getPais());
            String [] Coordenadas = b.getCoordenadas().split(";");
            String Latitud = Coordenadas[0];
            String Longitud = Coordenadas [1];
            System.out.println("Latitud: "+Latitud);
            System.out.println("Longitud: "+Longitud);
            System.out.println("Habitaciones: "+ b.getNumeroHabitaciones()+" "+b.getTipo()+"(Por: $"+b.getPrecio()+") Capacidad: "+b.getCapacidadHabitacion());
        }
        System.out.println("Ingrese una latitud: ");
        String latitud_buscar = sc.nextLine();
        System.out.println("Ingrese una longitud: ");
        String longitud_buscar = sc.nextLine();
        System.out.println("Ingrese un radio en kilometros: ");
        String radio = sc.nextLine();
        double lat1 = Double.parseDouble(latitud_buscar);
        double lon1 = Double.parseDouble(longitud_buscar);
        
        for(Alojamiento b:establecimientos){
            String [] Coordenadas = b.getCoordenadas().split(";");
            double lat2 = Double.parseDouble(Coordenadas[0]);
            double lon2 = Double.parseDouble(Coordenadas[1]);
            orden.add(CalcularDistancia(lat1,lat2,lon1,lon2));
        }
        Collections.sort(orden);
        
        
        System.out.println("Hoteles en su rango: ");
        for(Double c: orden){
            for(Alojamiento d:establecimientos){
                String [] Coordenadas = d.getCoordenadas().split(";");
                double lat2 = Double.parseDouble(Coordenadas[0]);
                double lon2 = Double.parseDouble(Coordenadas[1]);
                if(CalcularDistancia(lat1,lat2,lon1,lon2).equals(c)){
                    System.out.println(d.getNombreEstablecimiento()+": Estrellas: "+d.getEstrellas()+"-> Distancia: "+c+" m");
                
                }
            }
        }
        
        for(String f:leercsv){
            System.out.println(f);
 
        }
        
        
        
      }     
        
}
    public static Double CalcularDistancia(Double lat1,Double lat2, Double lon1,Double lon2){
            double earthRadius = 6371; // km
            lat1 = Math.toRadians(lat1);
            lon1 = Math.toRadians(lon1);
            lat2 = Math.toRadians(lat2);
            lon2 = Math.toRadians(lon2);
            double dlon = (lon2 - lon1);
            double dlat = (lat2 - lat1);
            double sinlat = Math.sin(dlat / 2);
            double sinlon = Math.sin(dlon / 2);
            double a = (sinlat * sinlat) + Math.cos(lat1)*Math.cos(lat2)*(sinlon*sinlon);
            double c = 2 * Math.asin (Math.min(1.0, Math.sqrt(a)));
            double distanceInMeters = earthRadius * c * 1000;
            return (distanceInMeters/1000);
    }
    
}
