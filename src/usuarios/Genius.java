/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package usuarios;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author LastSoul
 */
public class Genius extends Regular{
    private String numeroTarjeta;
    public Genius(String nombre, String email,String contrasena,String numeroTarjeta) {
        super(nombre, email, contrasena);
        this.numeroTarjeta=numeroTarjeta;
    }

    public String getNumeroTarjeta() {
        return numeroTarjeta;
    }

    public void setNumeroTarjeta(String numeroTarjeta) {
        this.numeroTarjeta = numeroTarjeta;
    }
    public static void anadirGenius(Genius u) {
		FileWriter flwriter = null;
		try {//además de la ruta del archivo recibe un parámetro de tipo boolean, que le indican que se va añadir más registros 
			flwriter = new FileWriter("Genius.txt", true);
			BufferedWriter bfwriter = new BufferedWriter(flwriter);
                        bfwriter.write(u.getNombre() + "," + u.getEmail() + "," + u.getContrasena() +","+u.getNumeroTarjeta()+ "\r\n");
			bfwriter.close();
			System.out.println("Archivo modificado satisfactoriamente..");
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (flwriter != null) {
				try {
					flwriter.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}	
    public static ArrayList leerArchivo(String archivo) {
		// crea el flujo para leer desde el archivo
		File file = new File(archivo);
		ArrayList usuariostxt= new ArrayList<>();	
		Scanner scanner;
		try {
			//se pasa el flujo al objeto scanner
			scanner = new Scanner(file);
			while (scanner.hasNextLine()) {
				// el objeto scanner lee linea a linea desde el archivo
				String linea = scanner.nextLine();
				Scanner delimitar = new Scanner(linea);
				//se usa una expresión regular
				//que valida que antes o despues de una coma (,) exista cualquier cosa
				//parte la cadena recibida cada vez que encuentre una coma				
				delimitar.useDelimiter("\\s*,\\s*");
				Genius e= new Genius("","","","");
				e.setNombre(delimitar.next());
				e.setEmail(delimitar.next());
				e.setContrasena(delimitar.next());
                                e.setNumeroTarjeta(delimitar.next());
				usuariostxt.add(e);
			}
			//se cierra el ojeto scanner
			scanner.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return usuariostxt;
	}
}
