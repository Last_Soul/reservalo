/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package usuarios;



/**
 *
 * @author LastSoul
 */
public class Usuarios {
    protected String nombre;
    protected String email;
    protected String contrasena;
    Usuarios(String nombre,String email,String contrasena){
    this.nombre=nombre;
    this.email=email;
    this.contrasena=contrasena;
}

    public String getNombre() {
        return nombre;
    }

    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}