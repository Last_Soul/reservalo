/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package usuarios;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author LastSoul
 */
public class Alojamiento extends Usuarios{ //usuarios que se registran como alojamientos
    static Scanner sc = new Scanner(System.in);
    public int ruc;
    public String nombreEstablecimiento;
    public String tipo;
    public String ciudad;
    public String pais;
    public int estrellas;
    public int numeroHabitaciones;
    public String coordenadas;
    public float precio;
    public int capacidadHabitacion;
    public int camas;
    public String servicios;
    public Alojamiento(String nombre,  String email,String contrasena, int ruc,String nombreEstablecimiento,String tipo,String ciudad,String pais,int estrellas,int numeroHabitaciones, String coordenadas, float precio, int capacidadHabitacion,int camas,String servicios) {
        super(nombre, email,contrasena);
        this.ruc=ruc;
        this.nombreEstablecimiento=nombreEstablecimiento;
        this.tipo=tipo;
        this.ciudad=ciudad;
        this.pais=pais;
        this.estrellas=estrellas;
        this.numeroHabitaciones=numeroHabitaciones;
        this.coordenadas=coordenadas;
        this.precio=precio;
        this.capacidadHabitacion=capacidadHabitacion;
        this.camas=camas;
        this.servicios=servicios;
    }
        public static void anadirAlojamiento(Alojamiento u) {
		FileWriter flwriter = null;
		try {//además de la ruta del archivo recibe un parámetro de tipo boolean, que le indican que se va añadir más registros 
			flwriter = new FileWriter("Alojamiento.txt", true);
			BufferedWriter bfwriter = new BufferedWriter(flwriter);
                        bfwriter.write(u.getNombre() + "," + u.getEmail() + "," + u.getContrasena() +","+u.getRuc()+","+u.getNombreEstablecimiento()+","+u.getTipo()+","+u.getCiudad()+","+u.getPais()+","+u.getEstrellas()+","+u.getNumeroHabitaciones()+","+u.getCoordenadas()+","+u.getPrecio()+","+u.getCapacidadHabitacion()+","+u.getCamas()+","+u.getServicios()+ "\r\n");
			bfwriter.close();
			System.out.println("Archivo modificado satisfactoriamente..");
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (flwriter != null) {
				try {
					flwriter.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
        

   

    public static Scanner getSc() {
        return sc;
    }

    public static void setSc(Scanner sc) {
        Alojamiento.sc = sc;
    }

    public int getRuc() {
        return ruc;
    }

    public void setRuc(int ruc) {
        this.ruc = ruc;
    }

    public String getNombreEstablecimiento() {
        return nombreEstablecimiento;
    }

    public void setNombreEstablecimiento(String nombreEstablecimiento) {
        this.nombreEstablecimiento = nombreEstablecimiento;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public int getEstrellas() {
        return estrellas;
    }

    public void setEstrellas(int estrellas) {
        this.estrellas = estrellas;
    }

    public int getNumeroHabitaciones() {
        return numeroHabitaciones;
    }

    public void setNumeroHabitaciones(int numeroHabitaciones) {
        this.numeroHabitaciones = numeroHabitaciones;
    }

    public String getCoordenadas() {
        return coordenadas;
    }

    public void setCoordenadas(String coordenadas) {
        this.coordenadas = coordenadas;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }

    public int getCapacidadHabitacion() {
        return capacidadHabitacion;
    }

    public void setCapacidadHabitacion(int capacidadHabitacion) {
        this.capacidadHabitacion = capacidadHabitacion;
    }

    public int getCamas() {
        return camas;
    }

    public void setCamas(int camas) {
        this.camas = camas;
    }

    public String getServicios() {
        return servicios;
    }

    public void setServicios(String servicios) {
        this.servicios = servicios;
    }
    
    public static ArrayList leerArchivo(String archivo) {
		// crea el flujo para leer desde el archivo
		File file = new File(archivo);
		ArrayList aloja= new ArrayList<>();	
		Scanner scanner;
		try {
			//se pasa el flujo al objeto scanner
			scanner = new Scanner(file);
			while (scanner.hasNextLine()) {
				// el objeto scanner lee linea a linea desde el archivo
				String linea = scanner.nextLine();
				Scanner delimitar = new Scanner(linea);
				//se usa una expresión regular
				//que valida que antes o despues de una coma (,) exista cualquier cosa
				//parte la cadena recibida cada vez que encuentre una coma				
				delimitar.useDelimiter("\\s*,\\s*");
				Alojamiento e= new Alojamiento("","","",1,"","","","",1,1,"",1,1,1,"");
				e.setNombre(delimitar.next());
				e.setEmail(delimitar.next());
				e.setContrasena(delimitar.next());
                                e.setRuc(Integer.parseInt(delimitar.next()));//string a int
                                e.setNombreEstablecimiento(delimitar.next());
                                e.setTipo(delimitar.next());
                                e.setCiudad(delimitar.next());
                                e.setPais(delimitar.next());
                                e.setEstrellas(Integer.parseInt(delimitar.next()));
                                e.setNumeroHabitaciones(Integer.parseInt(delimitar.next()));
                                e.setCoordenadas(delimitar.next());
                                e.setPrecio(Float.parseFloat(delimitar.next()));
                                e.setCapacidadHabitacion(Integer.parseInt(delimitar.next()));
                                e.setCamas(Integer.parseInt(delimitar.next()));
                                e.setServicios(delimitar.next());
				aloja.add(e);
			}
			//se cierra el ojeto scanner
			scanner.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return aloja;
	}
}
