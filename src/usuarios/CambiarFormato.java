/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package usuarios;

import java.io.*;
import java.util.ArrayList;
public class CambiarFormato {
/**
 *
 * @author Victor Alvarado
 */
  public static void main(String[] args) throws IOException {
    
   
    CambiarFormato archivos=new CambiarFormato();
    archivos.cambiarFormatoArchivo("atracciones.csv","atracciones.txt");
    archivos.cambiarFormatoArchivo("simplemaps-worldcities-basic.csv","simplemaps-worldcities-basic.txt");
     /*
   verificar
   ArrayList<String> arregloAtracciones = new ArrayList<String>();
   arregloAtracciones=archivos.crearArrays("atracciones.txt");
   
      */  
  }
   /*
    recibe un documento csv y el nombre para un nuevo documento txt, en este
    el metodo convertira de csv a txt 
    */
  public static void cambiarFormatoArchivo(String csvDoc,String nombreTxt) throws FileNotFoundException, IOException{
    String cadena;
    FileWriter fichero = null;
    PrintWriter pw = null;
    //Primero se realiza la lectura del archivo .csv
    File archivo = new File (csvDoc);
    FileReader fr = new FileReader (archivo);
    BufferedReader br = new BufferedReader(fr);  
    //para sobreescritura o creacion de un documento
    fichero = new FileWriter(nombreTxt);
    //por cada linea que se lee en el csv, se agrega al documento txt hasta que el documento no tenga mas lineas
    while((cadena = br.readLine())!=null) {
        pw = new PrintWriter(fichero);
        pw.println(cadena);
        } 
    fichero.close();
    fr.close();
    }
   //metodo que servira para crear un arreglo con el archivo ingresado
  public static ArrayList<String> crearArrays(String archivoTxt) throws IOException{
    
    FileReader fr = null;
    BufferedReader br = null;
    String[] cadena = null;
    ArrayList<String> datos = new ArrayList<String>();
    File    archivo = new File (archivoTxt);
         fr = new FileReader (archivo);
         br = new BufferedReader(fr);
         String linea;
         while((linea=br.readLine())!=null){
             datos.add(linea);
                        }
   fr.close();
   return datos;
  
 }
}
 
 

