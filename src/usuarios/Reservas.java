package usuarios;
import Metodos.Metodos;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author  
 */
public class Reservas extends Alojamiento{
    public static ArrayList<Reservas> reservas=  new ArrayList<Reservas>();
    String ciudad_reserva;
    int personas_reserva;
    String usuario_reserva; //usuario que realiza la reserva
    String date_inicio;
    String date_fin;
    public Reservas(String nombre, String email, String contrasena, int ruc, String nombreEstablecimiento, String tipo, String ciudad, String pais, int estrellas, int numeroHabitaciones, String coordenadas, float precio, int capacidadHabitacion, int camas, String servicios,String ciudad_reserva,int personas_reserva,String fecha_inicio,String fecha_fin) {
        super(nombre, email, contrasena, ruc, nombreEstablecimiento, tipo, ciudad, pais, estrellas, numeroHabitaciones, coordenadas, precio, capacidadHabitacion, camas, servicios);
        this.ciudad_reserva=ciudad_reserva;
        this.personas_reserva=personas_reserva;
        this.date_inicio=fecha_inicio;
        this.date_fin = fecha_fin;
    }


    
    public static void anadirReservas(Reservas u) {
    FileWriter flwriter = null;
		try {//además de la ruta del archivo recibe un parámetro de tipo boolean, que le indican que se va añadir más registros 
			flwriter = new FileWriter("Reservas.txt", true);
			BufferedWriter bfwriter = new BufferedWriter(flwriter);
                        bfwriter.write(u.getNombre() + "," + u.getEmail() + "," + u.getContrasena() +","+u.getRuc()+","+u.getNombreEstablecimiento()+","+u.getTipo()+","+u.getCiudad()+","+u.getPais()+","+u.getEstrellas()+","+u.getNumeroHabitaciones()+","+u.getCoordenadas()+","+u.getPrecio()+","+u.getCapacidadHabitacion()+","+u.getCamas()+","+u.getServicios()+","+u.getCiudad_reserva()+","+u.getPersonas_reserva()+","+u.getDate_inicio()+","+u.getDate_fin()+ "\r\n");
			bfwriter.close();
			System.out.println("Archivo modificado satisfactoriamente..");
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (flwriter != null) {
				try {
					flwriter.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
    }

    public static ArrayList<Reservas> getReservas() {
        return reservas;
    }

    public static void setReservas(ArrayList<Reservas> reservas) {
        Reservas.reservas = reservas;
    }

    public String getCiudad_reserva() {
        return ciudad_reserva;
    }

    public void setCiudad_reserva(String ciudad_reserva) {
        this.ciudad_reserva = ciudad_reserva;
    }

    public int getPersonas_reserva() {
        return personas_reserva;
    }

    public void setPersonas_reserva(int personas_reserva) {
        this.personas_reserva = personas_reserva;
    }

    public String getUsuario_reserva() {
        return usuario_reserva;
    }

    public void setUsuario_reserva(String usuario_reserva) {
        this.usuario_reserva = usuario_reserva;
    }

    public String getDate_inicio() {
        return date_inicio;
    }

    public void setDate_inicio(String date_inicio) {
        this.date_inicio = date_inicio;
    }

    public String getDate_fin() {
        return date_fin;
    }

    public void setDate_fin(String date_fin) {
        this.date_fin = date_fin;
    }
    public static ArrayList leerArchivo(String archivo) {
		// crea el flujo para leer desde el archivo
		File file = new File(archivo);
		ArrayList reser= new ArrayList<>();	
		Scanner scanner;
		try {
			//se pasa el flujo al objeto scanner
			scanner = new Scanner(file);
			while (scanner.hasNextLine()) {
				// el objeto scanner lee linea a linea desde el archivo
				String linea = scanner.nextLine();
				Scanner delimitar = new Scanner(linea);
							
				delimitar.useDelimiter("\\s*,\\s*");
				Reservas e= new Reservas("","","",1,"","","","",1,1,"",1,1,1,"","",1,"","");
				e.setNombre(delimitar.next());
				e.setEmail(delimitar.next());
				e.setContrasena(delimitar.next());
                                e.setRuc(Integer.parseInt(delimitar.next()));//string a int
                                e.setNombreEstablecimiento(delimitar.next());
                                e.setTipo(delimitar.next());
                                e.setCiudad(delimitar.next());
                                e.setPais(delimitar.next());
                                e.setEstrellas(Integer.parseInt(delimitar.next()));
                                e.setNumeroHabitaciones(Integer.parseInt(delimitar.next()));
                                e.setCoordenadas(delimitar.next());
                                e.setPrecio(Float.parseFloat(delimitar.next()));
                                e.setCapacidadHabitacion(Integer.parseInt(delimitar.next()));
                                e.setCamas(Integer.parseInt(delimitar.next()));
                                e.setServicios(delimitar.next());
                                e.setCiudad_reserva(delimitar.next());
                                e.setPersonas_reserva(Integer.parseInt(delimitar.next()));
                                e.setDate_inicio(delimitar.next());
                                e.setDate_fin(delimitar.next());
				reser.add(e);
			}
			//se cierra el ojeto scanner
			scanner.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return reser;
	}
}
    